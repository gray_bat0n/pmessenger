#!/usr/bin/env bash
mkdir $HOME/driver
mv ./chromedriver $HOME/driver/
PATH=$PATH:$HOME/driver/
python3 -m pip install -r requirements.txt
