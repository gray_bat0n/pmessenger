#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import requests
import os
import sys
from enum import Enum
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
import random
import time
import string

PORT = 17649
OK, CORRUPT, MUMBLE, DOWN, CHECKER_ERROR = 101, 102, 103, 104, 110
pswd = "7CROQVH03C"

DEBUG = os.getenv("DEBUG", False)
TRACE = os.getenv("TRACE", False)

def random_string(N=16, alph=string.ascii_lowercase + string.ascii_uppercase + string.digits):
    return ''.join([random.choice(alph) for _ in xrange(N)])

def sign_up(addr, pswd):
	options = Options()
	options.add_argument('--headless')
	options.add_argument('--disable-gpu') 
	driver = webdriver.Chrome("$HOME/chromedriver",chrome_options=options)
	driver.get("http://%s:17649/" % addr)

	name = driver.find_element(By.NAME,"name")
	psw = driver.find_element(By.NAME,"psw")
	
	name.clear()
	name.send_keys("admin")
	
	psw.clear()
	psw.send_keys(pswd)


	sup = driver.find_element(By.NAME,"sign_up")
	sup.click()
	driver.close()
	return request.post('http://%s:%d/' %(addr,PORT))

def store_post(addr,psw,p,user):
	options = Options()
	options.add_argument('--headless')
	options.add_argument('--disable-gpu') 
	driver = webdriver.Chrome("$HOME/chromedriver",chrome_options=options)
	name = driver.find_element(By.NAME,"name")
	psw = driver.find_element(By.NAME,"psw")
	name.send_keys("admin")

	psw.send_keys(psw)

	sin = driver.find_element(By.NAME,"sign_in")
	sin.click()
	time.sleep(5)

	postpriv = driver.find_element(By.NAME,"postpriv")
	postpriv.clear()
	postpriv.send_keys(p)
	sb_pr = driver.find_element(By.NAME,"submit_priv")
	sb_pr.click()
	driver.close()
	return request.post('http://%s:%d/' %(addr,PORT) ,data=p)

def check(addr):
	try:
		content=random_string(128)
		resp = store_post(addr, p=content)
		if resp is None:
			return DOWN
	except Exception as e:
		sys.stderr.write('[ERROR] %s\n' % e)
		return CHECKER_ERROR
	return OK


def sign_in(user,password):
	options = Options()
	options.add_argument('--headless')
	options.add_argument('--disable-gpu') 
	driver = webdriver.Chrome("$HOME/chromedriver",chrome_options=options)
	name = driver.find_element(By.NAME,"name")

	name.send_keys(user)

	psw.send_keys(pswd)
	sin = driver.find_element(By.NAME,"sign_in")
	
def get_post(host,psw,p,user):
	options = Options()
	options.add_argument('--headless')
	options.add_argument('--disable-gpu') 
	driver = webdriver.Chrome("$HOME/chromedriver",chrome_options=options)
	name = driver.find_element(By.NAME,"name")
	psw = driver.find_element(By.NAME,"psw")
	name.send_keys("admin")

	psw.send_keys(psw)

	sin = driver.find_element(By.NAME,"sign_in")
	sin.click()
	time.sleep(5)
	sin.click()
	rows = driver.find_elements(By.XPATH,"/html/body/table[4]/tbody/tr")
	number_of_rows = len(rows)
	priv_post = driver.find_element(By.XPATH,"/html/body/table[4]/tbody/tr[%d]/td/font" % (number_of_rows-1)).text 

	return priv_post

def put(addr,flag):
	try:
		resp = store_post(addr,pswd,flag,"admin")
		
		if resp is None:
			return DOWN

	except KeyError:
        	return MUMBLE
	except Exception as e:
        	sys.stderr.write('Exception: %s' % str(e))
        	return CHECKER_ERROR

def get(addr,flag):
	try:
		resp = get_post(addr,pswd,flag,user)
		if resp is None:
            		return DOWN
	except KeyError:
		return MUMBLE
	except Exception as e:
		return CHECKER_ERROR
	return OK



def _main():
	action, *args = sys.argv[1:]
	try:
		if action == "check":
			host, = args
			check(addr)
		elif action == "put":
			addr, flag = args
			put(addr, flag)
		elif action == "get":
			addr, flag = args
			get(addr,flag)
		else:
			raise IndexError
	except Exception as e:
 		return CHECKER_ERROR

	return OK


if __name__ == "__main__":
    _main()
