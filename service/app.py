#! /usr/bin/python3
import sqlite3
import os.path
from flask import Flask, render_template, render_template_string, request, flash, get_flashed_messages,redirect,url_for
from wtforms import StringField, TextAreaField, SubmitField
from wtforms.validators import DataRequired, Length
DB_PATH = './messages.db'
PORT = 17649

app = Flask(__name__)


def error_page(message, title='Error'):
	return render_template('error.html', message=message, title=title)

def get_user(name,psw):
	db = sqlite3.connect(DB_PATH)
	c = db.cursor()
	c.execute("SELECT * FROM user WHERE name=? and token=? LIMIT 1",(name,psw))
	res =c.fetchone()
	return res

def add_user(name,psw):
	db = sqlite3.connect(DB_PATH)
	c = db.cursor()
	c.execute("INSERT INTO user(token,name) VALUES (?,?)" , (psw,name))
	c.execute("SELECT user.id FROM user WHERE name='{}'".format(name))
	new_user_id= list(c.fetchone())[0]
	db.commit()
	return (new_user_id,psw,name)



@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index(title="SALAM"):
	if request.method == 'POST':
		if request.form.get("sign_in") == "Sign In":
			res=get_user(request.form['name'], request.form['psw'])
			if res:
				return redirect(url_for('get_main_page',id= res[0] ,token = request.form['psw'] ))
			else:
				flash("No user!")

		if request.form.get("sign_up") == "Sign Up":
			res = add_user(request.form['name'], request.form['psw'])
			if res:
				flash("success!!!")
			else:
				flash("Db error!")
		else:
			flash("Bad fields!")
	return render_template('welcome.html', title = title)


@app.route('/friends', methods=['GET', 'POST'])
def get_friends():
	user_id = int(request.args.get('id'))
	token_passed = str(request.args.get('token'))
	db = sqlite3.connect(DB_PATH)
	c = db.cursor()

	c.execute('SELECT user.name FROM user WHERE user.id = {}'.format(user_id))
	user_name = c.fetchall()

	c.execute('SELECT user.token FROM user WHERE user.id = {}'.format(user_id))
	token_real = str(c.fetchall()[0][0])

	if token_real != token_passed:
		return render_template_string(request.args.get('token') + ' is incorrect token!!!&#13;')

	c.execute('''SELECT user.name, user.id
		FROM user JOIN friendship on user.id = friendship.user_fk_1
		WHERE friendship.user_fk_2 = {} and friendship.state=1
		UNION
		SELECT user.name, user.id
		FROM user JOIN friendship on user.id = friendship.user_fk_2
		WHERE friendship.user_fk_1 = {} and friendship.state=1 '''.format(user_id, user_id))

	friend = c.fetchall()

	c.execute('''SELECT user.name, user.id
		FROM user JOIN friendship on user.id = friendship.user_fk_1
		WHERE friendship.user_fk_2 = {} and friendship.state=0
		UNION
		SELECT user.name, user.id
		FROM user JOIN friendship on user.id = friendship.user_fk_2
		WHERE friendship.user_fk_1 = {} and friendship.state=0 '''.format(user_id, user_id))
	friends_requests = c.fetchall()

	print(friend)
	c.execute('''SELECT user.name, user.id
		FROM user JOIN (
			SELECT id AS _id FROM user
			EXCEPT SELECT user_fk_1 FROM friendship WHERE user_fk_2 = {}
			EXCEPT SELECT user_fk_2 FROM friendship WHERE user_fk_1 = {}
			EXCEPT SELECT {}
		) ON user.id = _id'''.format(user_id, user_id, user_id))
	all_users = c.fetchall()
	for user in all_users:
		if request.form.get('Invite_{}'.format(user[1])) == "I":
			c = db.cursor()
			#c.execute('UPDATE friendship set state=0 where user_fk1={}'.format(user[1]))
			print(user_id,user[1],0)
			c.execute('INSERT INTO friendship VALUES (?, ?, ?)',(user_id,user[1],0))			
				#add unique to scheme
			db.commit()
		
	for user in friends_requests:
		if request.form.get('Apply_{}'.format(user[1])) == "A":
			print('a')
			c = db.cursor()
			c.execute('UPDATE friendship set state=1 where user_fk_2={} and user_fk_1={}'.format(user_id,user[1]))
			
			print(user_id,user[1])
			db.commit()

			
			print(user_id,user[1],c.fetchall())

		if request.form.get('Reject_{}'.format(user[1])) == "R":
			print('r')
			c = db.cursor()
			c.execute('UPDATE friendship set state=2 where user_fk_2={} and user_fk_1={}'.format(user_id,user[1]))		
			print(user_id,user[1])	
			db.commit()
			
	
	return render_template('friends.html', user_id=user_id, user_name=user_name,
		friend_requests=friends_requests,friends = friend,  all_users=all_users, token_passed=token_passed)

@app.route('/outcome', methods=['GET', 'POST'])
def get_outcome_messages():
	query = '''SELECT message.text
		FROM user JOIN message ON user.id = message.user_fk
		WHERE user.name = "{}" AND message.private = {}'''

	db = sqlite3.connect(DB_PATH)
	c = db.cursor()

	try:
		user_name = str(request.args.get('name'))
		token_passed = str(request.args.get('token'))

		c.execute('SELECT user.token FROM user WHERE user.name = "{}"'.format(user_name))
		token_real = str(c.fetchall()[0][0])

		if token_real != token_passed:
			return render_template_string(request.args.get('token') + ' is incorrect token!!!&#13;')			
			
		c.execute(query.format(user_name, 0))
		public_msg = c.fetchall()

		c.execute(query.format(user_name, 1))
		private_msg = c.fetchall()
	

		return render_template('outcome.html', username=user_name, private_msg=private_msg, public_msg=public_msg)

	except Exception as e:
		#print( username=user_name, private_msg=private_msg, public_msg=public_msg)
		return error_page(str(e), type(e))

	
@app.route('/income', methods=['GET', 'POST'])
def get_income_messages():
	user_id = request.args.get('id')
	db = sqlite3.connect(DB_PATH)
	c = db.cursor()

	try:
		token_passed = str(request.args.get('token'))

		c.execute('SELECT user.token FROM user WHERE user.id = {}'.format(user_id))
		token_real = str(c.fetchall()[0][0])

		if token_real != token_passed:
			return error_page('Incorrect token')

		c.execute('SELECT user.name FROM user WHERE user.id = {}'.format(user_id))
		user_name = c.fetchall()

		c.execute('''SELECT user.name, message.text
			FROM user JOIN message ON user.id = message.user_fk
			WHERE message.private = 0''')
		public_msg = c.fetchall()

		# BUG: user without friends does not see own private posts
		c.execute('''SELECT distinct user.name, message.text
			FROM friendship JOIN message ON message.user_fk = friendship.user_fk_1
				OR message.user_fk = friendship.user_fk_2
				JOIN user ON user.id = message.user_fk
			WHERE (friendship.user_fk_1 = {} OR friendship.user_fk_2 = {}) AND message.private = 1
			'''.format(user_id, user_id))
		private_msg = c.fetchall()

		return render_template('income.html', username=user_name, public_msg=public_msg, private_msg=private_msg)

	except Exception as e:
		return error_page(str(e), type(e))

@app.route('/post', methods=['POST', 'GET'])
def get_main_page():
	user_id = int(request.args.get('id'))
	token_passed = str(request.args.get('token'))
	db = sqlite3.connect(DB_PATH)
	c = db.cursor()
	c.execute('SELECT user.token FROM user WHERE user.id = {}'.format(user_id))
	token_real = str(c.fetchall()[0][0])

	if token_real != token_passed:
		return error_page('Incorrect token')


	c.execute('SELECT user.name FROM user WHERE user.id = {}'.format(user_id))
	user_name = c.fetchall()
	print(type(user_name[0]))
	friends_query = '''SELECT user.name, user.id
		FROM user JOIN friendship on user.id = friendship.user_fk_1
		WHERE friendship.user_fk_2 = {} AND friendship.state = 1
		UNION
		SELECT user.name, user.id
		FROM user JOIN friendship on user.id = friendship.user_fk_2
		WHERE friendship.user_fk_1 = {} AND friendship.state = 1'''.format(user_id, user_id)
	c.execute(friends_query)
	friends = c.fetchall()

	c.execute('''SELECT message.text, friend.name, friend.id
		FROM ({}) as friend JOIN message ON friend.id = message.user_fk
		WHERE message.private = 1
		UNION
		SELECT message.text, user.name, user.id
		FROM user JOIN message ON user.id = message.user_fk
		WHERE user.id = "{}" AND message.private = 1'''.format(friends_query, user_id))
	private_posts = c.fetchall()

	c.execute('''SELECT message.text, user.name, user.id
		FROM user JOIN message ON user.id = message.user_fk
		WHERE message.private = 0;
		''')
	public_posts = c.fetchall()

	if request.form.get("submit_pub") == "Submit_pub":
		mes = request.form['postpub']
		db = sqlite3.connect(DB_PATH)
		c = db.cursor()
		c.execute('INSERT INTO message(user_fk,text,private)  VALUES("{}", "{}", 0)'.format(user_id, mes))
		db.commit()
	if request.form.get("submit_priv") == "Submit_priv":
		mes = request.form['postpriv']
		db = sqlite3.connect(DB_PATH)
		c = db.cursor()
		c.execute('INSERT INTO message(user_fk,text,private)  VALUES("{}", "{}", 1)'.format(user_id, mes))
		db.commit()

	return render_template('post.html', user_id=user_id, user_name=list(user_name[0])[0], friends=friends,
		public_posts=public_posts, private_posts=private_posts,token_passed=token_passed)

if not os.path.exists(DB_PATH):
	db = sqlite3.connect(DB_PATH)
	c = db.cursor()
	c.execute('''CREATE TABLE user (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		token TEXT CHECK (LENGTH(token) BETWEEN 3 AND 30) NOT NULL,
		name TEXT UNIQUE CHECK (LENGTH(name) BETWEEN 3 AND 30) NOT NULL)''')

	c.execute('''CREATE TABLE friendship (
		user_fk_1 INTEGER REFERENCES users (id),
		user_fk_2 INTEGER REFERENCES users (id),
		state INTEGER CHECK (state IN (0, 1, 2)) NOT NULL,
		CONSTRAINT composite_pk PRIMARY KEY (user_fk_1, user_fk_2),
		CONSTRAINT no_self_loop CHECK (user_fk_1 <> user_fk_2))''')
	# state:
	#  0 -- unconfrimed. user_fk_1 is initiator, user_fk_2 should confrime
	#  1 -- confrimed
	#  2 -- rejected

	c.execute('''CREATE TABLE message (
		id INTEGER PRIMARY KEY AUTOINCREMENT,
		user_fk INTEGER REFERENCES users (id),
		text TEXT CHECK (LENGTH(text) < 1024) NOT NULL,
		private BOOLEAN NOT NULL)''')



app.secret_key = 'sup3r secr3t k3y'
app.run(host='0.0.0.0', port=PORT, debug=True)
